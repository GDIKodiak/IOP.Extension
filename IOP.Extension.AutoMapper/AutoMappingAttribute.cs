﻿using System;

namespace IOP.Extension.AutoMapper
{
    /// <summary>
    /// 自动映射标签
    /// </summary>
    public class AutoMappingAttribute : Attribute
    {
        /// <summary>
        /// 映射类型
        /// </summary>
        public Type SourceType { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="sourceType"></param>
        public AutoMappingAttribute(Type sourceType)
        {
            SourceType = sourceType;
        }
    }
}
