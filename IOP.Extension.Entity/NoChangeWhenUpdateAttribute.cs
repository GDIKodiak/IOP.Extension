﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace IOP.Extension.Entity
{
    /// <summary>
    /// 标识当更新数据时不应该更新此字段
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NoChangeWhenUpdateAttribute : Attribute
    {
    }
}
