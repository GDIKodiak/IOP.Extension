﻿using Microsoft.Extensions.DependencyInjection;

namespace IOP.Extension.Cache
{
    public static class MemoryCacheServiceExtension
    {
        /// <summary>
        /// 添加内存缓存
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddMemoryCacheService(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSingleton<ICacheService, MemoryCacheService>();
            return services;
        }
    }
}
