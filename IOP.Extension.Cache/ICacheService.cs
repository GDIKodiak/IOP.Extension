﻿using System;
using System.Collections.Generic;

namespace IOP.Extension.Cache
{
    public interface ICacheService
    {
        /// <summary>
        /// 判断缓存项是否存在
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        bool Exists(string key);

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        bool Set(string key, object value);
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="absoluteExpiration">绝对过期时间</param>
        /// <returns></returns>
        bool Set(string key, object value, DateTimeOffset absoluteExpiration);
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="absoluteExpiration">相对过期时间</param>
        /// <returns></returns>
        bool Set(string key, object value, TimeSpan expiresIn);
        /// <summary>
        /// 设置缓存(泛型方法)
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <param name=""></param>
        /// <returns></returns>
        bool Set<TItem>(string key, TItem item) where TItem : class;
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key">键</param>
        /// <param name="item">值</param>
        /// <param name="absoluteExpiration">绝对过期时间</param>
        /// <returns></returns>
        bool Set<TItem>(string key, TItem item, DateTimeOffset absoluteExpiration) where TItem : class;
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key">键</param>
        /// <param name="item">值</param>
        /// <param name="expiresIn">相对过期时间</param>
        /// <returns></returns>
        bool Set<TItem>(string key, TItem item, TimeSpan expiresIn) where TItem : class;


        /// <summary>
        /// 获取值
        /// </summary>
        /// <param name="key">键</param>
        /// <returns></returns>
        object Get(string key);
        /// <summary>
        /// 获取值
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key">键</param>
        /// <returns></returns>
        TItem Get<TItem>(string key);
        /// <summary>
        /// 尝试获取值
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key">键</param>
        /// <param name="item">值</param>
        void TryGetValue<TItem>(string key, out TItem item);
        /// <summary>
        /// 通过Key列表获取多个缓存项
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        IDictionary<string, object> GetRange(IEnumerable<string> keys);

        /// <summary>
        /// 移除一项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Remove(string key);
        /// <summary>
        /// 移除Key存在于参数列表中的项
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        void RemoveRange(IEnumerable<string> keys);
    }
}
