﻿using MessagePack;
using Microsoft.Extensions.Options;

namespace IOP.Extension.Cache
{
    public class CSRedisCacheOptions : IOptions<CSRedisCacheOptions>
    {
        /// <summary>
        /// 连接
        /// </summary>
        public string[] ConnectionStrings { get; set; }
        /// <summary>
        /// MessagePack序列化提供者
        /// </summary>
        public IFormatterResolver Resolver { get; set; } = MessagePack.Resolvers.ContractlessStandardResolver.Instance;

        public CSRedisCacheOptions Value => this;
    }
}
