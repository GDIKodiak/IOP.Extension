﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Extension.Cache
{
    public static class RedisCacheServiceExtensions
    {
        /// <summary>
        /// 使用Redis缓存服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IServiceCollection AddRedisCacheService(this IServiceCollection services, Action<CSRedisCacheOptions> options)
        {
            services.AddOptions<CSRedisCacheOptions>();
            services.Configure(options);
            services.AddSingleton<ICacheService, RedisCacheService>();
            return services;
        }
    }
}
