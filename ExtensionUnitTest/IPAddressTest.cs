﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IOP.Extension.Convert;
using System.Net;

namespace ExtensionUnitTest
{
    [TestClass]
    public class IPAddressTest
    {
        [TestMethod]
        public void IPTest()
        {
            string test1 = "10.36.112.37:4512";
            var ip = test1.ToIpAddress();
            Assert.AreEqual(ip.Address, IPAddress.Parse("10.36.112.37"));
            Assert.AreEqual(ip.Port, 4512);
            string test2 = "::6644:2332:3341:24a7:784d:468d:4512";
            var ip2 = test2.ToIpAddress();
            Assert.AreEqual(ip2.Address, IPAddress.Parse("::6644:2332:3341:24a7:784d:468d"));
            Assert.AreEqual(ip2.Port, 4512);
        }

    }
}
