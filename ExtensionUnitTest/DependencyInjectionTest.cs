﻿using IOP.Extension.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionUnitTest
{
    [TestClass]
    public class DependencyInjectionTest
    {
        [TestMethod]
        public void AutowiredTest()
        {
            try
            {
                IServiceCollection services = new ServiceCollection();
                services.AddTransient<ITestInterfaceB, TestB>();
                services.AddTransient<ITestInterfaceC, TestC>();
                IServiceProvider provider = services.BuildServiceProvider();
                var testA = provider.CreateAutowiredInstance<TestA>();
                Assert.IsTrue(testA.MehtodB() == "Hello World");
                Assert.IsTrue(testA.MethodA() == 100);
                var testAA = provider.CreateAutowiredInstance<TestA>();
                Assert.IsTrue(testAA.MethodC() == short.MaxValue);
                Assert.IsTrue(testAA.MethodD() == 123456789);
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [TestMethod]
        public void AutowiredTest2()
        {
            try
            {
                IServiceCollection services = new ServiceCollection();
                services.AddTransient<ITestInterfaceB, TestB>();
                services.AddTransient<ITestInterfaceC, TestC>();
                IServiceProvider provider = services.BuildServiceProvider();
                var testA = provider.CreateAutowiredInstance<TestA>();
                Assert.IsTrue(testA.MehtodB() == "Hello World");
                Assert.IsTrue(testA.MethodA() == 100);
                var testAA = provider.CreateAutowiredInstance(typeof(TestA)) as TestA;
                Assert.IsTrue(testAA.MethodC() == short.MaxValue);
                Assert.IsTrue(testAA.MethodD() == 123456789);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public interface ITestInterfaceA
    {
        int MethodA();

        string MehtodB();
    }

    public interface ITestInterfaceB
    {
        int GetInt();

        string GetString();
    }

    public interface ITestInterfaceC
    {
        short GetShort();

        long GetLong();
    }

    public class TestA : ITestInterfaceA
    {
        [Autowired]
        private ITestInterfaceB interfaceB { get; set; }

        [Autowired]
        private ITestInterfaceC interfaceC;

        public string MehtodB() => interfaceB.GetString();

        public int MethodA() => interfaceB.GetInt();

        public short MethodC() => interfaceC.GetShort();

        public long MethodD() => interfaceC.GetLong();
    }

    public class TestB : ITestInterfaceB
    {
        public int GetInt() => 100;

        public string GetString() => "Hello World";
    }

    public class TestC : ITestInterfaceC
    {
        public long GetLong() => 123456789;

        public short GetShort() => short.MaxValue;
    }
}
