﻿using IOP.Extension.Cache;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ExtensionUnitTest
{
    [TestClass]
    public class MemoryCacheTest
    {
        ICacheService Cache = null;

        public MemoryCacheTest()
        {
            var services = new ServiceCollection();
            services.AddMemoryCacheService();
            services.AddLogging();
            var provider = services.BuildServiceProvider();
            Cache = provider.GetRequiredService<ICacheService>();
        }


        [TestMethod]
        public void StringGetTest()
        {
            string test1 = "Redistest1";
            Cache.Set("test1", test1);
            Assert.AreEqual(test1, Cache.Get("test1"));
        }

        [TestMethod]
        public void ExistsTest()
        {
            string test1 = "Redistest1";
            Cache.Set("test1", test1);
            Assert.AreEqual(true, Cache.Exists("test1"));
        }

        [TestMethod]
        public void ListTest()
        {
            List<TestModel> tests = new List<TestModel>();
            for (int i = 0; i < 1000000; i++)
            {
                tests.Add(new TestModel { X = i, Y = i });
            }
            Cache.Set("test2", tests);
            var result = Cache.Get<List<TestModel>>("test2");
            Assert.AreEqual(1000000, result.Count);
        }

        [TestMethod]
        public void IsNullTest()
        {
            string test = Cache.Get<string>("test1202");
            Assert.IsNull(test);
        }

        [TestMethod]
        public void DeleteTest()
        {
            Cache.Set("test2", "666666");
            Cache.Remove("test2");
            Assert.IsFalse(Cache.Exists("test2"));
            Cache.Remove("test2");
        }

        [TestMethod]
        public void TimeTest()
        {
            TimeSpan span = DateTime.Now.AddSeconds(2) - DateTime.Now;
            Cache.Set("test3", "4545212", span);
            Thread.Sleep(3000);
            Assert.IsFalse(Cache.Exists("test3"));
        }
    }
}
