﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using IOP.Extension;

namespace ExtensionUnitTest
{
    [TestClass]
    public class ExpressionTest
    {
        [TestMethod]
        public void SetPropertyTest()
        {
            try
            {
                Test test = new Test();
                Test2 test2 = new Test2(125, "2323");
                test.Test2 = test2;
                test.SetPropertyValue("Id", 5);
                Assert.AreEqual(test.GetPropertyValue("Id"), 5);
                test.SetPropertyValue("Name", "Hello");
                Assert.AreEqual(test.GetPropertyValue("Name"), "Hello");
                test.SetPropertyValue("Name2", "World");
                Assert.AreEqual(test.GetPropertyValue("Name2"), "World");
                test.SetPropertyValue("Name", null);
                test.SetPropertyValue("Test2", null);
                Assert.IsNull(test.GetPropertyValue("Name"));
                Assert.IsNull(test.GetPropertyValue("Test2"));
                test.SetPropertyValue("Name", "TestTest");
                Assert.AreEqual(test.GetPropertyValue("Name"), "TestTest");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void AreEqualTest()
        {
            Test test1 = new Test();
            test1.Id = 288;
            test1.Name = "Hello";
            Test test2 = new Test();
            test2.Id = 4363;
            test2.Name = "Hello";
            Assert.IsTrue(test1.AreEqual(test2, "Name"));
            Assert.IsFalse(test1.AreEqual(test2, "Id"));
        }

        [TestMethod]
        public void CopyDataFromSourceTest()
        {
            Test test = new Test
            {
                Id = 6,
                Name = "1234",
                Test2 = new Test2(123, "123123")
            };
            Test test2 = new Test();
            test.CopyDataFromSource(test2);
            Assert.IsTrue(test.Id == test2.Id);
            Assert.IsTrue(test.Name == test2.Name);
            Assert.IsNotNull(test2.Test2);
        }
    }

    public class Test
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Name2 { get; private set; }

        public string Name3 { get; } = "H";

        public Test2 Test2 { get; set; }
    }

    public class Test2
    {
        public int A { get; set; }
        public string B { get; set; }

        public Test2(int a, string b)
        {
            A = a;
            B = b;
        }
    }
}
