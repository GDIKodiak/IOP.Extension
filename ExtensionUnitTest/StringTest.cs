﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.Extension.Convert;

namespace ExtensionUnitTest
{
    [TestClass]
    public class HexStringTest
    {
        private byte[] result1 = new byte[]
        {
            0x68, 0x01, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x68,
            0xA2, 0x32, 0x00, 0x36,
            0x33, 0x33, 0x36, 0x33,
            0x33, 0x3F, 0x33, 0x33,
            0x33, 0x3A, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x3A, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x33,
            0x33, 0x33, 0x33, 0x72,
            0xCF, 0x04, 0x79, 0x20,
            0x81, 0xE8, 0x16
        };
        private byte[] result2 = new byte[]
        {
            0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xff, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
            0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xff, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10
        };

        [TestMethod]
        public void HexStringToBytesTest()
        {
            var str1 = "6801000000000068A232003633333633333F3333333A333333333333333A3333333333333333333333333333333333333333333333333372CF04792081E816";
            var str2 = "0123456789abcdeffffedcba98765432100123456789ABCDEFFFFEDCBA9876543210";
            var str3 = "awdafwad123120pj12j";
            byte[] aa = str1.HexStringToBytes();
            byte[] bb = str2.HexStringToBytes();
            Assert.IsTrue(IsSame(aa, result1));
            Assert.IsTrue(IsSame(bb, result2));
            Assert.ThrowsException<Exception>(() =>
            {
                str3.HexStringToBytes();
            });
        }

        [TestMethod]
        public void StringToHexTest()
        {
            int index = 0;
            var str1 = "6801000000000068A232003633333633333F3333333A333333333333333A3333333333333333333333333333333333333333333333333372CF04792081E816";
            byte[] aa1 = str1.HexStringToBytes();
            ReadOnlySpan<byte> aa11 = aa1;
            string result1 = aa11.BytesToHexString(ref index, aa11.Length);
            Assert.AreEqual(str1, result1);
            index = 0;
            var str2 = "0123456789abcdeffffedcba98765432100123456789ABCDEFFFFEDCBA9876543210";
            byte[] aa2 = str2.HexStringToBytes();
            ReadOnlySpan<byte> aa22 = aa2;
            string result2 = aa22.BytesToHexString(ref index, aa22.Length);
            str2 = str2.ToUpper();
            Assert.AreEqual(str2, result2);
        }

        [TestMethod]
        public void StringToHexTest2()
        {
            int index = 0;
            var str1 = "68,01,00,00,00,00,00,68,A2,32,00,36,33,33,36,33,33,3F,33,33,33,3A,33,33,33,33,33,33,33,3A,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,33,72,CF,04,79,20,81,E8,16";
            byte[] aa1 = str1.HexStringToBytes(',');
            ReadOnlySpan<byte> aa11 = aa1;
            string result1 = aa11.BytesToHexString(ref index, aa11.Length, ',');
            Assert.AreEqual(str1, result1);
            index = 0;
            var str2 = "01 23 45 67 89 ab cd ef ff fe dc ba 98 76 54 32 10 01 23 45 67 89 AB CD EF FF FE DC BA 98 76 54 32 10";
            byte[] aa2 = str2.HexStringToBytes(' ');
            ReadOnlySpan<byte> aa22 = aa2;
            string result2 = aa22.BytesToHexString(ref index, aa22.Length, ' ');
            str2 = str2.ToUpper();
            Assert.AreEqual(str2, result2);
        }

        private bool IsSame(byte[] a, byte[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) return false;
            }
            return true;
        }
    }
}
