﻿using IOP.Extension.Convert;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace ExtensionUnitTest
{
    [TestClass]
    public class ConvertTest
    {

        [TestMethod]
        public void UshortWriteBytesTest()
        {
            ushort a = 0x1234;
            ushort b = 0x5678;
            ushort c = 0x9abc;
            ushort d = 0x0028;

            byte[] bab = new byte[] { 0x12, 0x34 };
            byte[] bal = new byte[] { 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x9a, 0xbc };
            byte[] ccl = new byte[] { 0xbc, 0x9a };
            byte[] ddb = new byte[] { 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00 };

            Span<byte> rab = new byte[2];
            Span<byte> ral = new byte[2];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[2];
            Span<byte> rbl = new byte[2];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[2];
            Span<byte> rcl = new byte[2];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[2];
            Span<byte> rdl = new byte[2];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }
        [TestMethod]
        public void ShortWriteBytesTest()
        {
            short a = 0x1234;
            short b = 0x5678;
            short c = 0x7123;
            short d = 0x0028;

            byte[] bab = new byte[] { 0x12, 0x34 };
            byte[] bal = new byte[] { 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x71, 0x23 };
            byte[] ccl = new byte[] { 0x23, 0x71 };
            byte[] ddb = new byte[] { 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00 };

            Span<byte> rab = new byte[2];
            Span<byte> ral = new byte[2];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[2];
            Span<byte> rbl = new byte[2];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[2];
            Span<byte> rcl = new byte[2];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[2];
            Span<byte> rdl = new byte[2];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }
        [TestMethod]
        public void UintWriteBytesTest()
        {
            uint a = 0x12341234;
            uint b = 0x56785678;
            uint c = 0x71237123;
            uint d = 0x00000028;

            byte[] bab = new byte[] { 0x12, 0x34, 0x12, 0x34 };
            byte[] bal = new byte[] { 0x34, 0x12, 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78, 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56, 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x71, 0x23, 0x71, 0x23 };
            byte[] ccl = new byte[] { 0x23, 0x71, 0x23, 0x71 };
            byte[] ddb = new byte[] { 0x00, 0x00, 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00, 0x00, 0x00 };

            Span<byte> rab = new byte[4];
            Span<byte> ral = new byte[4];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[4];
            Span<byte> rbl = new byte[4];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[4];
            Span<byte> rcl = new byte[4];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[4];
            Span<byte> rdl = new byte[4];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }
        [TestMethod]
        public void IntWriteBytesTest()
        {
            int a = 0x12345678;
            int b = 0x56785678;
            int c = 0x7123abcd;
            int d = 0x00000028;

            byte[] bab = new byte[] { 0x12, 0x34, 0x56, 0x78 };
            byte[] bal = new byte[] { 0x78, 0x56, 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78, 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56, 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x71, 0x23, 0xab, 0xcd };
            byte[] ccl = new byte[] { 0xcd, 0xab, 0x23, 0x71 };
            byte[] ddb = new byte[] { 0x00, 0x00, 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00, 0x00, 0x00 };

            Span<byte> rab = new byte[4];
            Span<byte> ral = new byte[4];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[4];
            Span<byte> rbl = new byte[4];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[4];
            Span<byte> rcl = new byte[4];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[4];
            Span<byte> rdl = new byte[4];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }
        [TestMethod]
        public void UlongWriteBytesTest()
        {
            ulong a = 0x123456789abcdef0;
            ulong b = 0x5678567856785678;
            ulong c = 0x7123abcd6febd731;
            ulong d = 0x0000000000000028;

            byte[] bab = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0 };
            byte[] bal = new byte[] { 0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78, 0x56, 0x78, 0x56, 0x78, 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56, 0x78, 0x56, 0x78, 0x56, 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x71, 0x23, 0xab, 0xcd, 0x6f, 0xeb, 0xd7, 0x31 };
            byte[] ccl = new byte[] { 0x31, 0xd7, 0xeb, 0x6f, 0xcd, 0xab, 0x23, 0x71 };
            byte[] ddb = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            Span<byte> rab = new byte[8];
            Span<byte> ral = new byte[8];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[8];
            Span<byte> rbl = new byte[8];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[8];
            Span<byte> rcl = new byte[8];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[8];
            Span<byte> rdl = new byte[8];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }
        [TestMethod]
        public void LongWriteBytesTest()
        {
            long a = 0x123456789abcdef0;
            long b = 0x5678567856785678;
            long c = 0x7123abcd6febd731;
            long d = 0x0000000000000028;

            byte[] bab = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0 };
            byte[] bal = new byte[] { 0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12 };
            byte[] bbb = new byte[] { 0x56, 0x78, 0x56, 0x78, 0x56, 0x78, 0x56, 0x78 };
            byte[] bbl = new byte[] { 0x78, 0x56, 0x78, 0x56, 0x78, 0x56, 0x78, 0x56 };
            byte[] ccb = new byte[] { 0x71, 0x23, 0xab, 0xcd, 0x6f, 0xeb, 0xd7, 0x31 };
            byte[] ccl = new byte[] { 0x31, 0xd7, 0xeb, 0x6f, 0xcd, 0xab, 0x23, 0x71 };
            byte[] ddb = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28 };
            byte[] ddl = new byte[] { 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            Span<byte> rab = new byte[8];
            Span<byte> ral = new byte[8];
            int index = 0;
            a.WriteBytes(ref rab, ref index);
            index = 0;
            a.WriteBytes(ref ral, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bab, rab.ToArray()));
            Assert.IsTrue(IsSame(bal, ral.ToArray()));

            Span<byte> rbb = new byte[8];
            Span<byte> rbl = new byte[8];
            b.WriteBytes(ref rbb, ref index);
            index = 0;
            b.WriteBytes(ref rbl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(bbb, rbb.ToArray()));
            Assert.IsTrue(IsSame(bbl, rbl.ToArray()));

            Span<byte> rcb = new byte[8];
            Span<byte> rcl = new byte[8];
            c.WriteBytes(ref rcb, ref index);
            index = 0;
            c.WriteBytes(ref rcl, ref index, Endian.LittleEndian);
            index = 0;
            Assert.IsTrue(IsSame(ccb, rcb.ToArray()));
            Assert.IsTrue(IsSame(ccl, rcl.ToArray()));

            Span<byte> rdb = new byte[8];
            Span<byte> rdl = new byte[8];
            d.WriteBytes(ref rdb, ref index);
            index = 0;
            d.WriteBytes(ref rdl, ref index, Endian.LittleEndian);
            Assert.IsTrue(IsSame(ddb, rdb.ToArray()));
            Assert.IsTrue(IsSame(ddl, rdl.ToArray()));
        }

        [TestMethod]
        public void UshortGetValueTest()
        {
            Span<byte> r1 = new byte[2];
            Span<byte> r2 = new byte[2];
            Span<byte> r3 = new byte[2];
            Span<byte> r4 = new byte[2];
            int index = 0;
            ushort x1 = ushort.MaxValue;
            ushort x2 = 12345;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out ushort d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out ushort d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out ushort d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out ushort d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void ShortGetValueTest()
        {
            Span<byte> r1 = new byte[2];
            Span<byte> r2 = new byte[2];
            Span<byte> r3 = new byte[2];
            Span<byte> r4 = new byte[2];
            int index = 0;
            short x1 = short.MaxValue;
            short x2 = short.MinValue;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out short d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out short d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out short d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out short d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void UintGetValueTest()
        {
            Span<byte> r1 = new byte[4];
            Span<byte> r2 = new byte[4];
            Span<byte> r3 = new byte[4];
            Span<byte> r4 = new byte[4];
            int index = 0;
            uint x1 = uint.MaxValue;
            uint x2 = 12345789;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out uint d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out uint d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out uint d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out uint d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void IntGetValueTest()
        {
            Span<byte> r1 = new byte[4];
            Span<byte> r2 = new byte[4];
            Span<byte> r3 = new byte[4];
            Span<byte> r4 = new byte[4];
            int index = 0;
            int x1 = int.MaxValue;
            int x2 = int.MinValue;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out int d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out int d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out int d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out int d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void LongGetValueTest()
        {
            Span<byte> r1 = new byte[8];
            Span<byte> r2 = new byte[8];
            Span<byte> r3 = new byte[8];
            Span<byte> r4 = new byte[8];
            int index = 0;
            long x1 = long.MaxValue;
            long x2 = long.MinValue;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out long d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out long d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out long d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out long d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void ULongGetValueTest()
        {
            Span<byte> r1 = new byte[8];
            Span<byte> r2 = new byte[8];
            Span<byte> r3 = new byte[8];
            Span<byte> r4 = new byte[8];
            int index = 0;
            ulong x1 = long.MaxValue;
            ulong x2 = 3464116546131216511;
            x1.WriteBytes(ref r1, ref index);
            index = 0;
            x1.WriteBytes(ref r2, ref index, Endian.LittleEndian);
            index = 0;
            x2.WriteBytes(ref r3, ref index);
            index = 0;
            x2.WriteBytes(ref r4, ref index, Endian.LittleEndian);
            index = 0;
            ReadOnlySpan<byte> t1 = r1;
            ReadOnlySpan<byte> t2 = r2;
            ReadOnlySpan<byte> t3 = r3;
            ReadOnlySpan<byte> t4 = r4;
            t1.GetValueFromBytes(ref index, out ulong d1);
            index = 0;
            t2.GetValueFromBytes(ref index, out ulong d2, Endian.LittleEndian);
            Assert.IsTrue(d1 == d2);
            Assert.IsTrue(d1 == x1);
            index = 0;
            t3.GetValueFromBytes(ref index, out ulong d3);
            index = 0;
            t4.GetValueFromBytes(ref index, out ulong d4, Endian.LittleEndian);
            Assert.IsTrue(d3 == d4);
            Assert.IsTrue(d3 == x2);
        }

        [TestMethod]
        public void UshortWriteBytesSpeedTest()
        {
            ushort[] testData = new ushort[10000000];
            Span<byte> local = new byte[sizeof(ushort) * 10000000];
            Random random = new Random();
            for(int i = 0; i < testData.Length; i++)
            {
                testData[i] = (ushort)(random.Next(ushort.MaxValue));
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for(int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }

        [TestMethod]
        public void ShortWriteBytesSpeedTest()
        {
            short[] testData = new short[10000000];
            Span<byte> local = new byte[sizeof(short) * 10000000];
            Random random = new Random();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i] = (short)(random.Next(short.MaxValue));
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }

        [TestMethod]
        public void UIntWriteBytesSpeedTest()
        {
            uint[] testData = new uint[10000000];
            Span<byte> local = new byte[sizeof(uint) * 10000000];
            Random random = new Random();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i] = (uint)(random.Next(int.MaxValue));
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }

        [TestMethod]
        public void IntWriteBytesSpeedTest()
        {
            int[] testData = new int[10000000];
            Span<byte> local = new byte[sizeof(int) * 10000000];
            Random random = new Random();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i] = (int)(random.Next(int.MaxValue));
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }

        [TestMethod]
        public void LongWriteBytesSpeedTest()
        {
            long[] testData = new long[10000000];
            Span<byte> local = new byte[sizeof(long) * 10000000];
            Random random = new Random();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i] = random.Next(int.MaxValue);
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }

        [TestMethod]
        public void ULongWriteBytesSpeedTest()
        {
            ulong[] testData = new ulong[10000000];
            Span<byte> local = new byte[sizeof(ulong) * 10000000];
            Random random = new Random();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i] = (ulong)(random.Next(int.MaxValue));
            }
            int index = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < testData.Length; i++)
            {
                testData[i].WriteBytes(ref local, ref index);
            }
            stopwatch.Stop();
            Debug.WriteLine($"time: {stopwatch.Elapsed.TotalMilliseconds}");
        }


        private bool IsSame<T>(T[] t1, T[] t2)
            where T : IComparable<T>
        {
            for(int i = 0; i < t1.Length; i++)
            {
                if (t1[i].CompareTo(t2[i]) != 0) return false;
            }
            return true;
        }

        private bool IsSame<T>(Span<T> t1, Span<T> t2)
            where T : struct, IComparable<T>
        {
            for (int i = 0; i < t1.Length; i++)
            {
                if (t1[i].CompareTo(t2[i]) != 0) return false;
            }
            return true;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    struct TestStruct
    {
        int A;
        int B;
        double C;
        string D;

        public TestStruct(int a, int b, double c, string d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
        }
    }
}
