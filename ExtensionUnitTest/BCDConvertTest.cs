﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.Extension.Convert;

namespace ExtensionUnitTest
{
    [TestClass]
    public class BCDConvertTest
    {
        [TestMethod]
        public void BCD8421Test()
        {
            try
            {
                int index = 0;
                ReadOnlySpan<byte> data1 = new byte[] { 0x96, 0x01, 0x00, 0x00 };
                ReadOnlySpan<byte> data11 = new byte[] { 0x00, 0x00, 0x01, 0x96 };
                double result1 = 1.96;
                data1.Get8421BCDFromBytes(ref index, data1.Length, out double c1, true, Endian.LittleEndian, 2);
                index = 0;
                data11.Get8421BCDFromBytes(ref index, data11.Length, out double c11, true, Endian.BigEndian, 2);
                Assert.AreEqual(result1, c1);
                Assert.AreEqual(result1, c11);
                index = 0;
                ReadOnlySpan<byte> data2 = new byte[] { 0x04, 0x08, 0x00, 0x00 };
                ReadOnlySpan<byte> data22 = new byte[] { 0x00, 0x00, 0x08, 0x04 };
                double result2 = 8.04;
                data2.Get8421BCDFromBytes(ref index, data2.Length, out double c2, true, Endian.LittleEndian, 2);
                index = 0;
                data22.Get8421BCDFromBytes(ref index, data2.Length, out double c22, true, Endian.BigEndian, 2);
                Assert.AreEqual(result2, c2);
                Assert.AreEqual(result2, c22);
                index = 0;
                ReadOnlySpan<byte> data3 = new byte[] { 0x96, 0x69, 0x77, 0x01 };
                ReadOnlySpan<byte> data33 = new byte[] { 0x01, 0x77, 0x69, 0x96 };
                double result3 = 177.6996;
                data3.Get8421BCDFromBytes(ref index, data3.Length, out double c3, true, Endian.LittleEndian, 4);
                index = 0;
                data33.Get8421BCDFromBytes(ref index, data33.Length, out double c33, true, Endian.BigEndian, 4);
                Assert.AreEqual(result3, c3);
                Assert.AreEqual(result3, c33);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void ExcessThreeCodeTest()
        {
            try
            {
                int index = 0;
                ReadOnlySpan<byte> data1 = new byte[] { 0xC9, 0x34, 0x33, 0x33 };
                ReadOnlySpan<byte> data11 = new byte[] { 0x33, 0x33, 0x34, 0xC9 };
                double result1 = 1.96;
                data1.GetExcessThreeCodeFromBytes(ref index, data1.Length, out double c1, true, Endian.LittleEndian, 2);
                index = 0;
                data11.GetExcessThreeCodeFromBytes(ref index, data11.Length, out double c11, true, Endian.BigEndian, 2);
                Assert.AreEqual(result1, c1);
                Assert.AreEqual(result1, c11);
                index = 0;
                ReadOnlySpan<byte> data2 = new byte[] { 0x37, 0x3B, 0x33, 0x33 };
                ReadOnlySpan<byte> data22 = new byte[] { 0x33, 0x33, 0x3B, 0x37 };
                double result2 = 8.04;
                data2.GetExcessThreeCodeFromBytes(ref index, data2.Length, out double c2, true, Endian.LittleEndian, 2);
                index = 0;
                data22.GetExcessThreeCodeFromBytes(ref index, data2.Length, out double c22, true, Endian.BigEndian, 2);
                Assert.AreEqual(result2, c2);
                Assert.AreEqual(result2, c22);
                index = 0;
                ReadOnlySpan<byte> data3 = new byte[] { 0xC9, 0x93, 0xAA, 0x34 };
                ReadOnlySpan<byte> data33 = new byte[] { 0x34, 0xAA, 0x93, 0xC9 };
                double result3 = 177.6096;
                data3.GetExcessThreeCodeFromBytes(ref index, data3.Length, out double c3, true, Endian.LittleEndian, 4);
                index = 0;
                data33.GetExcessThreeCodeFromBytes(ref index, data33.Length, out double c33, true, Endian.BigEndian, 4);
                Assert.AreEqual(result3, c3);
                Assert.AreEqual(result3, c33);
                index = 0;
                Assert.ThrowsException<Exception>(() => 
                {
                    ReadOnlySpan<byte> data4 = new byte[] { 0xC9, 0x9C, 0xAA, 0x34 };
                    data4.Get8421BCDFromBytes(ref index, data4.Length, out double c4, true, Endian.LittleEndian, 4);
                });
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void ExcessThreeCodeTest2()
        {
            int index = 0;
            ReadOnlySpan<byte> data1 = new byte[] { 0x68, 0x88, 0x39, 0x33 };
            ReadOnlySpan<byte> data11 = new byte[] { 0x33, 0x39, 0x88, 0x68 };
            ushort result1 = 65535;
            data1.GetExcessThreeCodeFromBytes(ref index, data1.Length, out ushort c1, true, Endian.LittleEndian);
            index = 0;
            data11.GetExcessThreeCodeFromBytes(ref index, data11.Length, out ushort c11, true, Endian.BigEndian);
            Assert.AreEqual(result1, c1);
            Assert.AreEqual(result1, c11);
            index = 0;
            ReadOnlySpan<byte> data2 = new byte[] { 0xC3, 0xBB, 0xAA, 0x99 };
            ReadOnlySpan<byte> data22 = new byte[] { 0x99, 0xAA, 0xBB, 0xC3 };
            uint result2 = 66778890;
            data2.GetExcessThreeCodeFromBytes(ref index, data2.Length, out uint c2, true, Endian.LittleEndian);
            index = 0;
            data22.GetExcessThreeCodeFromBytes(ref index, data2.Length, out uint c22, true, Endian.BigEndian);
            Assert.AreEqual(result2, c2);
            Assert.AreEqual(result2, c22);
            index = 0;
            ReadOnlySpan<byte> data3 = new byte[] { 0xC3, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x43, 0x33 };
            ReadOnlySpan<byte> data33 = new byte[] { 0x33, 0x43, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xC3 };
            ulong result3 = 102233445566778890;
            data3.GetExcessThreeCodeFromBytes(ref index, data3.Length, out ulong c3, true, Endian.LittleEndian);
            index = 0;
            data33.GetExcessThreeCodeFromBytes(ref index, data33.Length, out ulong c33, true, Endian.BigEndian);
            Assert.AreEqual(result3, c3);
            Assert.AreEqual(result3, c33);
            index = 0;
            ReadOnlySpan<byte> data4 = new byte[] { 0x9A, 0x38, 0x56, 0x34 };
            ReadOnlySpan<byte> data44 = new byte[] { 0x34, 0x56, 0x38, 0x9A };
            uint result4 = 1230567;
            data4.GetExcessThreeCodeFromBytes(ref index, data4.Length, out uint c4, true, Endian.LittleEndian);
            index = 0;
            data44.GetExcessThreeCodeFromBytes(ref index, data44.Length, out uint c44, true, Endian.BigEndian);
            Assert.AreEqual(result4, c4);
            Assert.AreEqual(result4, c44);
            index = 0;
        }

        [TestMethod]
        public void ExcessThreeCodeTest3()
        {
            int index = 0;
            ReadOnlySpan<byte> data1 = new byte[] { 0x33, 0x33, 0x34, 0x33 };
            ReadOnlySpan<byte> data11 = new byte[] { 0x33, 0x34, 0x33, 0x33 };
            decimal result1 = 1.0000M;
            data1.GetExcessThreeCodeFromBytes(ref index, 4, out decimal d1, true, Endian.LittleEndian, 4);
            Assert.AreEqual(result1, d1);
            index = 0;
            data11.GetExcessThreeCodeFromBytes(ref index, 4, out decimal d2, true, Endian.BigEndian, 4);
            Assert.AreEqual(result1, d2);
            index = 0;
            ReadOnlySpan<byte> data2 = new byte[] { 0x34, 0x46, 0x38, 0x34, 0x36 };
            ReadOnlySpan<byte> data22 = new byte[] { 0x36, 0x34, 0x38, 0x46, 0x34 };
            float result2 = 3010.51301f;
            data2.GetExcessThreeCodeFromBytes(ref index, 5, out float f1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result2, f1);
            index = 0;
            data22.GetExcessThreeCodeFromBytes(ref index, 5, out float f2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result2, f2);
            index = 0;
            ReadOnlySpan<byte> data3 = new byte[] { 0x34, 0x46, 0x38, 0x34, 0x36 };
            ReadOnlySpan<byte> data33 = new byte[] { 0x36, 0x34, 0x38, 0x46, 0x34 };
            double result3 = 3010.51301;
            data3.GetExcessThreeCodeFromBytes(ref index, 5, out double dd1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result3, dd1);
            index = 0;
            data33.GetExcessThreeCodeFromBytes(ref index, 5, out double dd2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result3, dd2);
            index = 0;
            ReadOnlySpan<byte> data4 = new byte[] { 0x34, 0x46, 0x38, 0x34, 0x36 };
            ReadOnlySpan<byte> data44 = new byte[] { 0x36, 0x34, 0x38, 0x46, 0x34 };
            decimal result4 = 3010.51301M;
            data4.GetExcessThreeCodeFromBytes(ref index, 5, out decimal ddd1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result4, ddd1);
            index = 0;
            data44.GetExcessThreeCodeFromBytes(ref index, 5, out decimal ddd2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result4, ddd2);
            index = 0;
        }

        [TestMethod]
        public void ExcessThreeCodeTest4()
        {
            int index = 0;
            ReadOnlySpan<byte> data1 = new byte[] { 0x33, 0x33, 0x34, 0x33, 0x34 };
            ReadOnlySpan<byte> data11 = new byte[] { 0x34, 0x33, 0x34, 0x33, 0x33 };
            decimal result1 = 1000.10000M;
            data1.GetExcessThreeCodeFromBytes(ref index, 5, out decimal d1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result1, d1);
            index = 0;
            data11.GetExcessThreeCodeFromBytes(ref index, 5, out decimal d2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result1, d2);
            index = 0;
            double result2 = 1000.10000;
            data1.GetExcessThreeCodeFromBytes(ref index, 5, out double dd1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result2, dd1);
            index = 0;
            data11.GetExcessThreeCodeFromBytes(ref index, 5, out double dd2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result2, dd2);
            index = 0;
            float result3 = 1000.10000f;
            data1.GetExcessThreeCodeFromBytes(ref index, 5, out float f1, true, Endian.LittleEndian, 5);
            Assert.AreEqual(result3, f1);
            index = 0;
            data11.GetExcessThreeCodeFromBytes(ref index, 5, out float f2, true, Endian.BigEndian, 5);
            Assert.AreEqual(result3, f2);
            index = 0;
        }
    }
}
