﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace IOP.Extension
{
    /// <summary>
    /// 加密扩展
    /// </summary>
    public static class EncryptExtension
    {
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="encryptString">待加密的内容</param>
        /// <returns></returns>
        public static string MD5Encrypt(this string encryptString)
        {
            if (string.IsNullOrEmpty(encryptString)) throw (new ArgumentNullException(nameof(encryptString)));
            MD5 m_ClassMD5 = new MD5CryptoServiceProvider();
            string m_strEncrypt = "";
            try
            {
                m_strEncrypt = BitConverter.ToString(m_ClassMD5.ComputeHash(Encoding.Default.GetBytes(encryptString))).Replace("-", "");
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_ClassMD5.Clear();
            }
            return m_strEncrypt;
        }
    }
}
