﻿namespace IOP.Extension.Convert
{
    /// <summary>
    /// 字节序
    /// </summary>
    public enum Endian
    {
        /// <summary>
        /// 大端字节序
        /// </summary>
        BigEndian,
        /// <summary>
        /// 小端字节序
        /// </summary>
        LittleEndian
    }
}
