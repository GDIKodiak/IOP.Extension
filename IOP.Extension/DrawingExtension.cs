﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace IOP.Extension
{
    /// <summary>
    /// Drawing相关扩展
    /// </summary>
    public static class DrawingExtension
    {
        /// <summary>
        /// 将16进制码转换为RGB
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static Color TryHx16ToRgb(this string code)
        {
            try
            {
                if (code.Length == 0)
                {
                    return Color.FromArgb(0, 0, 0);
                }
                else
                {
                    code.Replace("#", "");
                    return Color.FromArgb(int.Parse(code.Substring(1, 2),
                        System.Globalization.NumberStyles.AllowHexSpecifier),
                        int.Parse(code.Substring(3, 2), System.Globalization.NumberStyles.AllowHexSpecifier),
                        int.Parse(code.Substring(5, 2), System.Globalization.NumberStyles.AllowHexSpecifier));
                }
            }
            catch
            {
                return Color.FromArgb(0, 0, 0);
            }
        }
    }
}
