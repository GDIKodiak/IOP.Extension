﻿using System;

namespace IOP.Extension.DependencyInjection
{
    /// <summary>
    /// 自动注入标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class AutowiredAttribute : Attribute
    {
    }
}
