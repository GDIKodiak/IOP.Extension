﻿using Xunit;
using Microsoft.Extensions.DependencyInjection;
using IOP.Extension.Entity;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace IOP.Entension.Entity.UnitTest
{
    public class EntityUnitTest : TestBase
    {
        [Fact]
        public async Task MyTestMethod()
        {
            var testDb = Provider.GetService<TestDbContext>();
            TestA test = new TestA()
            {
                TestAA = "asda",
                TestBB = 15331513
            };
            test = await testDb.CreateOrUpdateAsync(test);
            test.TestAA = "awdawgawfdawgawfa";
            test = await testDb.CreateOrUpdateAsync(test);
            Assert.True(test.TestAA == "awdawgawfdawgawfa");
        }

        [Fact]
        public async Task DeleteTest1()
        {
            var testDb = Provider.GetService<TestDbContext>();
            TestA test = new TestA()
            {
                TestAA = "asdasadawfwawd",
                TestBB = 1533151312512
            };
            test = await testDb.CreateOrUpdateAsync(test);
            await testDb.DeleteAsync("TestA", test.Id);
            var test2 = await testDb.TestA.Where(x => x.Id == test.Id).FirstOrDefaultAsync();
            Assert.Null(test2);
        }

        [Fact]
        public async Task DeleteTest2()
        {
            var testDb = Provider.GetService<TestDbContext>();
            TestC test = new TestC()
            {
                Id = Guid.NewGuid(),
                CCC = "awdafawdawfawd",
                DDDD = "awfawgawwwdasdaw"
            };
            testDb.Add(test);
            testDb.SaveChanges();
            await testDb.DeleteAsync("TestC", test.Id);
            var test2 = await testDb.TestC.Where(x => x.Id == test.Id).FirstOrDefaultAsync();
            Assert.Null(test2);

            TestD testd = new TestD()
            {
                Id = Guid.NewGuid().ToString(),
                ADF = "awdawfawd",
                ADWA = "awfawgawf"
            };
            testDb.Add(testd);
            testDb.SaveChanges();
            await testDb.DeleteAsync("TestD", test.Id);
            var test3 = await testDb.TestC.Where(x => x.Id == test.Id).FirstOrDefaultAsync();
            Assert.Null(test3);
        }

        [Fact]
        public async Task DeleteRangeTest1()
        {
            var testDb = Provider.GetService<TestDbContext>();
            TestC test = new TestC()
            {
                Id = Guid.NewGuid(),
                CCC = "awdafawdawf23awd1",
                DDDD = "awfawgawwwdasdaw1"
            };
            TestC test2 = new TestC
            {
                Id = Guid.NewGuid(),
                CCC = "awdafawdawf23awd1",
                DDDD = "awfawgawwwdasdaw1"
            };
            testDb.Add(test);
            testDb.Add(test2);
            testDb.SaveChanges();
            await testDb.DeleteRangeAsync("TestC", new List<Guid> { test.Id, test2.Id });
            var result1 = await testDb.TestC.Where(x => x.Id == test.Id).FirstOrDefaultAsync();
            var result2 = await testDb.TestC.Where(x => x.Id == test2.Id).FirstOrDefaultAsync();
            Assert.Null(result1);
            Assert.Null(result2);
            TestD test3 = new TestD
            {
                Id = Guid.NewGuid().ToString(),
                ADF = "awdawfawd",
                ADWA = "awfawgawf"
            };
            TestD test4 = new TestD
            {
                Id = Guid.NewGuid().ToString(),
                ADF = "awdawfawd",
                ADWA = "awfawgawf"
            };
            testDb.Add(test3);
            testDb.Add(test4);
            testDb.SaveChanges();
            await testDb.DeleteRangeAsync("TestD", new List<string> { test3.Id, test4.Id });
            var result3 = await testDb.TestD.Where(x => x.Id == test3.Id).FirstOrDefaultAsync();
            var result4 = await testDb.TestD.Where(x => x.Id == test4.Id).FirstOrDefaultAsync();
            Assert.Null(result3);
            Assert.Null(result4);
        }

        [Fact]
        public async Task QueryTest()
        {
            var testDb = Provider.GetService<TestDbContext>();
            var sql = @"select * from TestA";
            var tests = await testDb.QueryAsync<TestA>(sql);
            var sql2 = "select * from TestB";
            var tests2 = await testDb.QueryAsync<TestB>(sql2);
        }
    }
}
