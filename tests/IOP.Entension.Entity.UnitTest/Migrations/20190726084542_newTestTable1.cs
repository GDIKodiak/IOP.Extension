﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IOP.Entension.Entity.UnitTest.Migrations
{
    public partial class newTestTable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TestC",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CCC = table.Column<string>(nullable: true),
                    DDDD = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestD",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ADF = table.Column<string>(nullable: true),
                    ADWA = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestD", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestC");

            migrationBuilder.DropTable(
                name: "TestD");
        }
    }
}
