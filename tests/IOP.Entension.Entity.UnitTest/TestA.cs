﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IOP.Entension.Entity.UnitTest
{
    public class TestA
    {
        [Key]
        public int Id { get; set; }

        public string TestAA { get; set; }

        public long TestBB { get; set; }
    }
}
