﻿using IOP.Extension.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Xunit;

namespace ExtensionJsonTest
{
    public class JSONConvertTest
    {
        [Theory]
        [InlineData(@"{""Connections"":[{""Device"":[""PC"",""Tablet""]},{""Device"":[""PC"",""Laptop""]}]}")]
        [InlineData(@"{""Connections"":[{""Device"":[""Tablet"",""PC""]},{""Device"":[""Laptop"",""PC""]}]}")]
        public void EnumArray(string json)
        {
            ConnectionList obj;

            void Verify()
            {
                Assert.Equal(2, obj.Connections.Count);
                Assert.Equal(eDevice.PC | eDevice.Tablet, obj.Connections[0].Device);
                Assert.Equal(eDevice.PC | eDevice.Laptop, obj.Connections[1].Device);
            }

            obj = JsonSerializer.Deserialize<ConnectionList>(json);
            Verify();

            // Round-trip and verify.
            json = JsonSerializer.Serialize(obj);
            obj = JsonSerializer.Deserialize<ConnectionList>(json);
            Verify();
        }

        [Theory]
        [InlineData(@"{""Connections"":[{""Device"":[""PC"",""Tablet""]},{""Device"": ""PC""}]}")]
        [InlineData(@"{""Connections"":[{""Device"":[""Tablet"",""PC""]},{""Device"": ""PC""}]}")]
        public void EnumArrayTest2(string json)
        {
            ConnectionList obj;
            void Verify()
            {
                Assert.Equal(2, obj.Connections.Count);
                Assert.Equal(eDevice.PC | eDevice.Tablet, obj.Connections[0].Device);
                Assert.Equal(eDevice.PC, obj.Connections[1].Device);
            }

            obj = JsonSerializer.Deserialize<ConnectionList>(json);
            Verify();

            json = JsonSerializer.Serialize(obj);
            obj = JsonSerializer.Deserialize<ConnectionList>(json);
            Verify();
        }

        [Theory]
        [InlineData(@"{""Devices"":[[""PC"",""Tablet""],""Studio""]}")]
        public void EnumArrayTest3(string json)
        {
            Connection2 obj;
            void Verify()
            {
                Assert.Equal(2, obj.Devices.Length);
                Assert.Equal(eDevice.PC | eDevice.Tablet, obj.Devices[0]);
                Assert.Equal(eDevice.Studio, obj.Devices[1]);
            }

            obj = JsonSerializer.Deserialize<Connection2>(json);
            Verify();

            json = JsonSerializer.Serialize(obj);
            obj = JsonSerializer.Deserialize<Connection2>(json);
            Verify();
        }
    }

    [Flags]
    public enum eDevice : uint
    {
        Unknown = 0x0,
        Phone = 0x1,
        PC = 0x2,
        Laptop = 0x4,
        Tablet = 0x8,
        IoT = 0x10,
        Watch = 0x20,
        TV = 0x40,
        Hub = 0x80,
        Studio = 0x100,
        Book = 0x200,
        MediaCenter = 0x400,
    }

    public class Connection2
    {
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public eDevice[] Devices { get; set; }
    }

    public class ConnectionList
    {
        public List<ConnectionFile> Connections { get; set; }
    }

    public class ConnectionFile
    {
        [JsonConverter(typeof(JsonStringEnumArrayConverter))]
        public eDevice Device { get; set; }
    }
}
